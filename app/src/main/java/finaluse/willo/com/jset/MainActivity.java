package finaluse.willo.com.jset;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    SimpleAdapter adapter;
    EditText input,other;
    Spinner spinner;
    ArrayList<HashMap<String, String>> formList;
    final Activity act = this;
    ListView fields;
    SharedPreferences SP;
    String messagetitle = "Email From: ";


    private static String url = "http://gdriv.es/finaluse/template.json";
    JSONArray user = null;

    //JSON Node Names
    private static final String TAG_USER = "fields";
//    private static final String TAG_ID = "id";
//    private static final String TAG_NAME = "name";
//    private static final String TAG_EMAIL = "email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SP= PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        if (!SP.contains("username")) {
            Intent i = new Intent(this, AppPreferences.class);
            startActivity(i);
        }
        new JSONParse().execute();
        fields = (ListView)findViewById(R.id.listView);
//        // Just for testing, allow network access in the main thread
//        // NEVER use this is productive code
//        StrictMode.ThreadPolicy policy = new StrictMode.
//                ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
        formList = new ArrayList<HashMap<String, String>>();
        adapter = new SimpleAdapter(this, formList,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "value" },
                new int[] {android.R.id.text1, android.R.id.text2 });

        fields.setAdapter(adapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Sending Email", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            sendEmail();

            }
        });



        fields.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position,
                                    long id) {

                final String item = ((TwoLineListItem) view).getText1().getText().toString();

                LinearLayout layout = new LinearLayout(MainActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                alert.setTitle(item);
               // alert.setMessage("Enter Value");
                ;
                JSONObject jo  = getJSONObject(item);
                if (jo!=null)
                {
                    Log.d("JSONTEST", jo.toString());

                    try {
                        Log.d("JSONTEST", jo.getString("type"));
                        if (jo.getString("type").equals("spinner")) {
                            ArrayList<String> optionslist = new ArrayList<String>();
                            JSONArray jsonarray = jo.getJSONArray("options");
                            for (int i = 0; i < jsonarray.length(); i++) {
                                optionslist.add(jsonarray.getString(i));
                            }
                            spinner = new Spinner(MainActivity.this);
                            spinner
                                    .setAdapter(new ArrayAdapter(MainActivity.this,
                                            android.R.layout.simple_spinner_dropdown_item,
                                            optionslist));

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                    // your code here
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parentView) {
                                    // your code here
                                }

                            });
                            input = null;
                            layout.addView(spinner);
                            other = new EditText(MainActivity.this);
                            other.setHint("Other");
                            layout.addView(other);
                            alert.setView(layout);
                            Log.d("JSONTEST", "spinner setup");
                        }
                        else if (jo.getString("type").equals("email")) {
                            // Seperate Intent.EXTRA_EMAIL into strings + create a spinner
                            List<String> emails = Arrays.asList(SP.getString("email","no email set").split(","));
                            ArrayList<String> optionslist = new ArrayList<String>();
                            for (String email:emails)
                            {
                                optionslist.add(email);
                            }
                            Log.d("JSONTEST", optionslist.toString());
                            spinner = new Spinner(MainActivity.this);
                            spinner.setId(R.id.email);
                            spinner
                                    .setAdapter(new ArrayAdapter(MainActivity.this,
                                            android.R.layout.simple_spinner_dropdown_item,
                                            optionslist));

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parentView) {
                                    // your code here
                                }

                            });
                            input = null;
                            layout.addView(spinner);
                            other = new EditText(MainActivity.this);
                            other.setHint("Other");
                            layout.addView(other);
                            alert.setView(layout);
                        }
                        else {
                            input = new EditText(MainActivity.this);
                            layout.addView(input);
                            alert.setView(layout);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        input = new EditText(MainActivity.this);
                        layout.addView(input);
                        alert.setView(layout);
                    }
                }
                else {
                    input = new EditText(MainActivity.this);
                    layout.addView(input);
                    alert.setView(layout);
                }

                // Set an EditText view to get user input

                //input = new Spinner(MainActivity.this);
                //input.setInputType(InputType.TYPE_CLASS_NUMBER| InputType.TYPE_NUMBER_VARIATION_PASSWORD);


                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            String value;

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if(input!=null)
                                    value = input.getText().toString();
                                else if (other.getText().toString().equals("")) {
                                    value = spinner.getSelectedItem().toString();
                                }
                                else
                                    value = other.getText().toString();
                                HashMap<String, String> hashitem = ((HashMap<String, String>) fields.getAdapter().getItem(position));
                                // Do something with value!
//                                try {
                                Snackbar.make(fields, item + " set to " + value, Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
//                                }
//                                catch (NullPointerException e)
//                                {
//                                    Snackbar.make(view, "ERROR: " + e.getMessage(), Snackbar.LENGTH_LONG)
//                                            .setAction("Action", null).show();
//                                }

                                String title = hashitem.get("title");
                                formList.remove(position);
                                HashMap<String, String> m_li = new HashMap<String, String>();
                                m_li.put("title", title);
                                m_li.put("value", value);
                                formList.add(position, m_li);
                                adapter.notifyDataSetChanged();

                                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


                            }
                        }
                );

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
//                        Snackbar.make(fields, "Input Cancelled", Snackbar.LENGTH_LONG)
//                                .setAction("Action", null).show();
                        //((TwoLineListItem) view).getText2().setText(new String("cancelled"));
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });

                alert.show();

            }
        });



}


    public JSONObject getJSONObject(String _name)
    {
        String name;
        JSONObject searchObject =null;

        JSONArray array = null;
        try {
            array = user;

            for (int i = 0; i < array.length(); i++) {
                JSONObject currObject = array.getJSONObject(i);
                name = currObject.getString("title");

                if(name == _name)
                {
                    searchObject = currObject;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchObject;
    }

private void sendEmail()
{
    Intent emailintent = new Intent(Intent.ACTION_SEND);
    emailintent.setType("message/rfc822");

    String body = new String();
    body += "Name: "  + SP.getString("username","no username set") + '\n';
    //= '\n'+ "Name : "+ etname.getText().toString() + '\n'+
     //       "Description : "+etdesc.getText().toString() + '\n'+
     //       "Skill Required : "+etskill.getText().toString() + '\n'+
     //       "Site : "+etsite.getText().toString() + '\n'+
     //       "Site Status : "+ etstatus.getText().toString() + '\n'+
     //       "Repair Date : "+ etdate.getText().toString();
    String email = "error";
    for (int i = 0; i < fields.getCount(); i++) {
        TextView title = (TextView) fields.getChildAt(i).findViewById(android.R.id.text1);
        TextView data = (TextView) fields.getChildAt(i).findViewById(android.R.id.text2);
        if (title!=null && data!=null) {
            Log.d("SENDEMAIL",(String) title.getText());
            if(((String)title.getText()).contains("Recipient"))
            {
               email = (String)data.getText();
            }
            else {
                body += title.getText() + " : " + data.getText() + '\n';
            }

        }
    }
    emailintent.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
    emailintent.putExtra(Intent.EXTRA_SUBJECT, messagetitle  + SP.getString("username","no username set"));
    emailintent.putExtra(Intent.EXTRA_TEXT   , body);
    try {
        startActivity(Intent.createChooser(emailintent, "Send mail..."));
    } catch (android.content.ActivityNotFoundException ex) {
        Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
    }
}




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, AppPreferences.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class JSONParse extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Log.d("JSONTEST",values.toString());
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser(act);

            // Getting JSON from URL
            Log.d("JSONTEST", "backgrounding");
            JSONObject json = jParser.getJSON(SP.getString("json_loc", "default.json"));
            Log.d("JSONTEST","DONE");
            return json;
        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {
                try {
                    // Getting JSON Array
                    Log.d("JSONTEST","POST EXECUTE");
                    //JSONObject c = json;
                    user = jsonObject.getJSONArray(TAG_USER);
                    try {
                        //JSONObject obj = new JSONObject(loadJSONfromUrl());

                        //while (user == null){;} // super hacky
                        JSONArray m_jArry = user;

                        HashMap<String, String> m_li;

                        for (int i = 0; i < m_jArry.length(); i++) {
                            JSONObject jo_inside = m_jArry.getJSONObject(i);

                            String title = jo_inside.getString("title");
                            String value = jo_inside.getString("value");
                            if (title.equals("@messagetitle"))
                            {
                                messagetitle = value;
                            }
                            else {

                                //Add your values in your `ArrayList` as below:
                                m_li = new HashMap<String, String>();
                                m_li.put("title", title);
                                if (title.equals("Recipient"))
                                    m_li.put("value", Arrays.asList(SP.getString("email","no email set").split(",")).get(0));
                                else
                                    m_li.put("value", value);
                                formList.add(m_li);
                            }


                        }
                        ;

                    adapter.notifyDataSetChanged();


                    }
                    catch (JSONException e)
                    {
                        Log.d("JSONTEST", "Parsing actual json");
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("JSONTEST", "JSON JSONException");
                }


        }
    }
}
